
namespace tiled {
    export class Text extends egret.TextField {




        constructor(x: number, y: number, width: number, height: number, data: tiled.ITMXText) {
            super();
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;

            this.textColor = data.color ? TMXUtils.color16ToUnit(data.color) : 0;
            this.text = data.text;
            this.size = data.pixelsize ?? 16;

            this.italic = data.italic;
            this.bold = data.bold;



        }


        draw() {


        }



    }
}