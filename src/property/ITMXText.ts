namespace tiled {
    export interface ITMXText {



        /**
         * 文本颜色
         */
        color: string;

        /**
         * 文本行数
         */
        wrap: number;

        /**
         * 文本内容
         */
        text: string;

        /**
         * 文本字体
         */
        fontfamily: string;



        /**
         * 字体大小
         */
        pixelsize: number;

        /**
         * 是否为斜体
         */
        italic: boolean;

        /**
         * 是否有下划线
         */
        underline: boolean;


        /**
         * 是否有删除线
         */
        strikeout: boolean;

        /**
         * 是否加粗
         */
        bold: boolean;

    }
}